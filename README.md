INTRODUCTION

 * The Admin CSS module allows site builders to add CSS through the admin
 dashboard. For a full description of the module please visit:
 https://www.drupal.org/project/admincss.


REQUIREMENTS

 * This module does not have any module dependencies


INSTALLATION

 * Install the Admin CSS module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.

CONFIGURATION

 * Navigate to Administration > Extend and enable the module.

 * Navigate to Administration » Configuration » Development » Admin CSS Editor 
   to add custom css.

 * Save your configuration. When enabled, the module will allows users to
   add css in the admin dashboard.

 * To remove the Admin CSS, disable the module and clear caches.

MAINTAINERS

Current maintainer:

 * Damodharan (damu) - https://www.drupal.org/user/3308527
 * codebymikey - https://www.drupal.org/user/3573206

This project has been sponsored by:

 * Pepper Square Software Services Private Limited specialized in software
   product development and planning of Drupal powered sites.

 * Pepper Square offers complete development cycle and hosting to get you start.
   Visit: https://www.peppersquare.com for more information.

 * Zodiac Media - maintenance and bug fixes for the project.
   Visit https://www.zodiacmedia.co.uk for more information.
 
 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/admincss
